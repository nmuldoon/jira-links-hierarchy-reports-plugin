/*
 * Copyright (c) 2007, Pyxis-Technologies inc.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();

var hierarchy = new function()
{
	this.graphs= $M();
	this.registerGraph=function(gUID){ 
		this.graphs[gUID] = new JIRAGraph(gUID);
		return this.graphs[gUID];
	}
	this.registerIssue=function(gUID, iKey, lId, vId, subtasks, sequence, resolved, treeView, themeName, ctx, view, isTarget, orphans)
	{
		var graph = this.getGraph(gUID);
		if(!graph){ graph = this.registerGraph(gUID);}
		return graph.registerIssue($H({decorator:'none', printView:($('printView')?true:false), bulkUID:gUID, issueKey:iKey, versionId:vId, linkTypeId:lId, subtasks:subtasks, treeView:treeView, sequence:sequence, themeName:themeName, ctx:ctx, view:view, orphans:orphans}), isTarget, resolved);
	};	
	this.getGraph=function(gUID){ return this.graphs[gUID];	};
	this.getIssue=function(gUID, sequence){ return this.getGraph(gUID).getIssue(sequence); };
	this.refreshAll=function(){
		this.graphs.eachValue(function(value){
			value.refresh();
		});
	};
	this.overrideCancel=function(gUID, sequence){
		if($('cancelButton')){
			$('cancelButton').onclick=function(){ 
			 	hierarchy.getIssue(gUID, sequence).closeWorkflowInputs();
			};
		}
	};
};

var JIRAGraph = Class.create();
JIRAGraph.prototype = 
{
	initialize:function(gUID){
		this.issues= $M();
		this.id=gUID;
		this.progressBar=new ProgressBar('jira_progressBar_'+this.id);
	},
	
	getIssue:function(sequence)	{ return this.issues[sequence]; },
	registerIssue:function(params, isTarget, resolved){
		var issue = this.issues[params.sequence] = new JIRAIssue(this, params, isTarget, resolved);
		if(resolved) this.progressBar.addResolved(issue);
		else this.progressBar.addUnresolved(issue);
		return issue;
	},
	refresh:function(){ 
		this.progressBar.reset();
		this.drawHierarchy($('isTreeView_'+this.id)? true:false);
	},
	drawHierarchy:function(treeView){
		this.progressBar.reset();
		var issue = this.getTargetedIssue();
		if(treeView){Pyxis.Util.switchView('org_link_'+this.id, 'tree_link_'+this.id);}
		else{Pyxis.Util.switchView('tree_link_'+this.id, 'org_link_'+this.id);}
		new Ajax.Updater('graph_'+this.id, issue.params.ctx+"/secure/IssueCell.jspa", issue.query(issue, {treeView:treeView}));
	},
	getTargetedIssue:function(){
		var result;
		this.issues.eachValue(function(issue){
			if(issue.isTarget){
				result = issue; throw $break;
			}
		});
		return result;
	}
};

var JIRAIssue = Class.create();
JIRAIssue.prototype =
{
	initialize:function(graph, params, isTarget, resolved){
		this.graph = graph;
		this.params=params;
		this.isTarget=isTarget;
		this.resolved=resolved;
		this.id='_'+this.params.bulkUID+'_'+this.params.issueKey+'_'+this.params.sequence;
		
	},
	getVersionHierarchy:function(projectId, versionId){
		Pyxis.Util.hide('jira_hierarchy_actionError'+this.id);
		var hParams = this.params.merge({decorator:'', selectedProjectId:projectId, versionId:versionId, reportKey:'com.pyxis.jira.links.hierarchy.reports:pyxis.hierarchy.report.version', Next:'Next'});
		var sAction= this.params.ctx+"/secure/ConfigureReport.jspa?"+hParams.toQueryString();
		document.forms[0].action=sAction;
		document.forms[0].submit();
	},
	getIssueHierarchy:function(projectId){
		Pyxis.Util.hide('jira_hierarchy_actionError'+this.id);
		var hParams = this.params.merge({decorator:'', selectedProjectId:projectId, reportKey:'com.pyxis.jira.links.hierarchy.reports:pyxis.hierarchy.report.issue', Next:'Next'});
		var sAction= this.params.ctx+"/secure/ConfigureReport.jspa?"+hParams.toQueryString();
		document.forms[0].action=sAction;
		document.forms[0].submit();
	},
	bulkEdit:function(){
		Pyxis.Util.switchView('jira_hierarchy_waiting_display'+this.id, 'jira_hierarchy_actionError'+this.id);
		var sAction= this.params.ctx+"/secure/LinkBulkEdit.jspa?"+this.params.toQueryString();
		document.forms[0].action=sAction;
		document.forms[0].submit();
	},
	parents:function() { new Ajax.Updater('parents'+this.id, this.params.ctx+"/secure/Parents.jspa", this.query(this)); },
	children:function() { new Ajax.Updater('children'+this.id, this.params.ctx+"/secure/Children.jspa", this.query(this)); },
	expand:function(){
		Pyxis.Util.show('children'+this.id);
		Pyxis.Util.switchView('collapse'+this.id, 'expand'+this.id);
	},	
	collapse:function(){
		Pyxis.Util.hide('children'+this.id);
		Pyxis.Util.switchView('expand'+this.id, 'collapse'+this.id);
	},
	openActionsPopup:function(){
		this.working();
		this.closeAllPopups('workflow_actions', this.id);
		var pos = Pyxis.Util.findPos($('jira_actionsLink'+this.id));
		var popup = this.getPopup('jira_actionsPopup'+this.id, (pos[1] - (BrowserDetect.browser == 'Firefox' ? 5 : 0)), (pos[0] + 22), "1000");
		Effect.BlindDown(popup, {duration:0.4,queue:'parallel'});
		new Ajax.Updater('jira_actionsPopup'+this.id, this.params.ctx+"/secure/WorflowActions.jspa", this.query(this));
	},
	openLinkPopup:function(){
		this.working();
		this.closeAllPopups('add_links', this.id);
		$('linkKey'+this.id).value = '';
		$('comment'+this.id).value = '';
		var pos = Pyxis.Util.findPos($('jira_issueLink'+this.id));
		var popup = this.getPopup('jira_linkPopup'+this.id, (pos[1] - (BrowserDetect.browser == 'Firefox' ? 5 : 0)), (pos[0] + 22), "1000");
		Effect.BlindDown(popup, {duration:0.4,queue:'parallel'});
		this.doneWorking();
	},
	openBreakLinkPopup:function(linkType){
		this.working();
		this.closeAllPopups('break_links', this.id, linkType);
	    var pos = Pyxis.Util.findPos($(linkType+this.id)); 
		var popup = this.getPopup(linkType+'_link'+this.id, (Math.max(pos[1] - 40, 20)), (Math.max(pos[0] - 85, 20)), "1000");
		Pyxis.Util.appear(linkType+'_link'+this.id, 0.3);
		this.doneWorking();
	},
	openWorkflowInputs:function(url){
		this.closeActionPopup();
		var pos = Pyxis.Util.findPos($('jira_actionsLink'+this.id));
		var popup = this.getPopup('jira_workflow_inputs'+this.id, (pos[1] - (BrowserDetect.browser == 'Firefox' ? 5 : 0)), (pos[0] + 22), "1000");
		Effect.BlindDown(popup, {duration:0.4,queue:'parallel'});
		new Ajax.Updater('jira_workflow_inputs'+this.id, url);
	},
	openBreakTooltip:function(linkType){
		var issuekeys = document.getElementsByClassName('lh_breakbox', linkType+'_break'+this.id);
		if(issuekeys.length > 1)
			for (var counter=0; counter<issuekeys.length; counter++)
				issuekeys[counter].checked = 'checked';

		Pyxis.Util.appear(linkType+'_linkTooltip'+this.id, 0.3);
	},
	getPopup:function(popupId, top, left, z){
		var popup = $(popupId);
		popup.style.top = top;
		popup.style.left = left;
		popup.style.position = "absolute";
		popup.style.visibility = "visible";
		popup.style.zIndex = z;
		return popup;		
	},
	closeAllPopups:function(popup, linkType){
		if(popup != 'workflow_actions') this.closeActionPopup();
		if(popup != 'add_links') this.closeLinkPopup();
		if(popup != 'workflow_inputs') this.closeWorkflowInputs();
		if(popup != 'break_links'){
			if(linkType == 'inward') this.closeBreakLinkPopup('outward');
			else this.closeBreakLinkPopup('inward');
		}
	},
	closeActionPopup:function() { if($('jira_actionsPopup'+this.id)) Effect.BlindUp('jira_actionsPopup'+this.id, {duration:0.4,queue:'parallel'}); },
	closeLinkPopup:function(){if($('jira_linkPopup'+this.id)) Effect.BlindUp('jira_linkPopup'+this.id, {duration:0.4,queue:'parallel'}); },
	closeBreakLinkPopup:function(linkType) { Pyxis.Util.fade(linkType+'_link'+this.id, 0.3); },
	closeBreakTooltip:function(linkType) {
		if(linkType){
			Pyxis.Util.fade(linkType+'_linkTooltip'+this.id, 0.3);
		}
		else{
			Pyxis.Util.fade('inward_linkTooltip'+this.id, 0.3);
			Pyxis.Util.fade('outward_linkTooltip'+this.id, 0.3);
		} 
	},
	closeWorkflowInputs:function() {
		if($('jira_workflow_inputs'+this.id)) Effect.BlindUp('jira_workflow_inputs'+this.id, {duration:0.4,queue:'parallel'});
		setTimeout("Pyxis.Util.write('jira_workflow_inputs"+this.id+"', '<div align=center><img src=\""+ this.params.ctx +"/download/resources/com.pyxis.jira.links.hierarchy.reports:pyxis.hierarchy.report.issue/images/waiting3.gif\"></div>')", 400);
	},
	changeStatus:function(id, action){
		this.working();
		new Ajax.Updater('jira_hierarchy_actionError'+this.id, this.params.ctx+"/secure/HierarchyWorkflow.jspa", this.query(this, {id:id, action:action}));
	},
	statusChanged:function(resolved){
		this.working();
		this.resolved = resolved;
		this.graph.progressBar.statusChanged(this);
		this.closeActionPopup();
		new Ajax.Updater('jira_issueSummary'+this.id, this.params.ctx+"/secure/IssueCellSummary.jspa", this.query(this));

	},
	addLink:function(){
		var linkKey = this.trim($F('linkKey'+this.id));
		if(linkKey == '') return;
		this.working();	
		var linkParams = {linkDesc:$F('linkDesc'+this.id), linkKey:linkKey, comment:$F('comment'+this.id)};
		new Ajax.Updater('jira_hierarchy_actionError'+this.id, this.params.ctx+"/secure/AddLinks.jspa", this.query(this, linkParams));
	},
	trim:function(s){
		s = s.replace( /^\s+/g, "" );
		return s.replace( /\s+$/g, "" );
	},
	breakLinks:function(linkDesc, inward){
		this.working();
		var linkKey = '';
		var issuekeys = inward ? document.getElementsByClassName('lh_breakbox', 'inward_break'+this.id) : document.getElementsByClassName('lh_breakbox', 'outward_break'+this.id);
		for (var counter=0; counter<issuekeys.length; counter++)
			if (issuekeys[counter].checked == true)
				linkKey = linkKey + (counter == 0 ? '':', ') + issuekeys[counter].value;
		
		this.closeBreakLinkPopup(inward?'inward':'outward');
		new Ajax.Updater('jira_hierarchy_actionError'+this.id, this.params.ctx+"/secure/BreakLinks.jspa", this.query(this, {linkDesc:linkDesc,linkKey:linkKey}));
	},
	working:function(){
		Pyxis.Util.show('jira_hierarchy_waiting_display'+this.id);
		Pyxis.Util.hide('jira_hierarchy_actionError'+this.id);
	},
	doneWorking:function(){Pyxis.Util.fade('jira_hierarchy_waiting_display'+this.id, 1);},
	error:function(XMLHttpRequest, err){},
	query: function(obj, addParams){ return {method:'get', parameters:(addParams ? obj.params.merge(addParams) : obj.params).toQueryString(), onComplete: obj.doneWorking.bind(obj), onException:obj.error.bind(obj), onFailure:obj.error.bind(obj), evalScripts:true};}
};

function ProgressBar(displayId)
{
	this.issues= $M();
	this.displayId = displayId;
	this.totalTics = 0; this.resolved = 0; this.unresolved = 0;
	this.fontSize = 8;

	this.reset = function(draw){ 
		if($(this.displayId)){ 
			this.issues.clear();
			this.totalTics = 0; this.resolved = 0; this.unresolved = 0;
			if(draw)this.draw();
		}
	};	
	this.addResolved = function(issue){
		 if(this.resolved <= this.totalTics && !this.issues[issue.params.issueKey]){
		 	this.issues[issue.params.issueKey] = issue.resovled;
		 	this.resolved++;
		 }
		 this.draw();
	};
	this.addUnresolved = function(issue){ 
		if(this.unresolved <= this.totalTics && !this.issues[issue.params.issueKey]){
		 	this.issues[issue.params.issueKey] = issue.resolved;
			this.unresolved++;
		}
		this.draw();
	};
	this.statusChanged = function(issue){ 
		if(this.issues[issue.params.issueKey] == issue.resolved) return;
		else if(issue.resolved) { this.unresolved--; this.resolved++; }
		else { this.unresolved++; this.resolved--; }
		this.draw();
	};
	this.removeUnresolved = function(){ if(this.failures){this.failures--;} this.draw(); };
	this.draw = function(){
		var inHTML = "<table style=\"border:1px solid #bbbbbb; border-spacing:0px; width:100%; margin: 0px; padding: 0px;\" cellspacing=0px cellpadding=0px><tr>";
		var limit = this.totalTics - this.resolved - this.unresolved;
		if(limit < 0){this.totalTics = this.resolved + this.unresolved;}
		for(var i = 0; i < this.resolved; i++){inHTML =  inHTML + "<td style=\"font-size: "+this.fontSize+"px; padding:0px 10px 0px 0px; font-weight:bold; font-family: Arial, sans-serif; text-align: center; white-space: nowrap; vertical-align:middle;background-color:#009900;\">&nbsp;</td>";}
		for(var j = 0; j < this.unresolved; j++){inHTML =  inHTML + "<td style=\"font-size: "+this.fontSize+"px; padding:0px 10px 0px 0px; font-weight:bold; font-family: Arial, sans-serif; text-align: center; white-space: nowrap; vertical-align:middle;background-color:#BD0000;\">&nbsp;</td>";}
		for(var z = 0; z < limit; z++){inHTML =  inHTML + "<td style=\"font-size: "+this.fontSize+"px; padding:0px 10px 0px 0px; font-weight:bold; font-family: Arial, sans-serif; text-align: center; white-space: nowrap; vertical-align:middle;background-color:#D0D0D0;\">&nbsp;</td>";}
	 	Pyxis.Util.write(this.displayId, inHTML + '</tr></table>');
		Pyxis.Util.write('stats_'+this.displayId, this.resolved+'/'+this.totalTics);
	};
};