/*
 * Copyright (c) 2007, Pyxis-Technologies inc.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

var Pyxis = { VERSION: '1.1' };
Pyxis.Util =
{
	show: function(id){ var element = $(id); if(element){Element.show(element);element=null;} },
	hide: function(id){ var element = $(id); if(element){Element.hide(element);element=null;} },
	switchView: function(switchShow, switchHide){ this.hide(switchHide); this.show(switchShow); },
	isVisible: function(id){ var element = $(id); var visible=(element && Element.visible(element)); element=null; return visible;},
	innerValue: function(id){ var element = $(id); if(element){var value = (element ? element.innerHTML : ''); element=null; return value;}},
	write: function(id, content){ var element = $(id); if(element){element.innerHTML = content; element=null;}},
	appear: function(id, time){ var element = $(id); if(element){new Effect.Appear(id, {duration:time,queue:'parallel'}); element=null;}},
	fade: function(id, time){ var element = $(id); if(element){new Effect.Fade(id, {duration:time,queue:'parallel'}); element=null;}},
	highLight: function(id, time){ var element = $(id); if(element){new Effect.Highlight(element, {duration:time,queue:'parallel'}); element=null;}},
	setClassName: function(id, className){ var element = $(id); if(element){element.className = className; element=null;}},
	destroy:function(id){ var element = $(id); if(element){ element.remove(); element=null;}},
	trim:function(s){ if(!s)return ""; s = s.replace( /^\s+/g, "" ); return s.replace( /\s+$/g, "" ); },
	findPos: function(obj){ var curleft = curtop = 0; if(obj.offsetParent) { curleft = obj.offsetLeft; curtop = obj.offsetTop; while (obj = obj.offsetParent) { curleft += obj.offsetLeft; curtop += obj.offsetTop; } } return [curleft,curtop]; },
	getPopup:function(popupId, left, top, z){ var popup = $(popupId); popup.style.top = top; popup.style.left = left; popup.style.position = "absolute"; popup.style.visibility = "visible"; popup.style.zIndex = z; return popup; },
	toDate:function(s){ var d = '';	for(var i=0; i<s.length; i++){ if((s.charAt(i) >= "0") && (s.charAt(i) <= "9")){ var j=i; while((s.charAt(j) >= "0") && (s.charAt(j) <= "9") && j<s.length) d=d+s.charAt(j++); d=d+s.charAt(j+1)+' '; i=j+2; } } return d; },
	toggle:function(id){var element = $(id); if(element){if($F(element)){element.checked='';}else{element.checked='checked';} element=null;}},
	setValue: function(id, value) {if ($(id)) $(id).value = value; },
	inputFocus: function(element, className){ element.className = className; element.value = ''; },
	windownDim: function() {
	  var myWidth = 0, myHeight = 0;
	  if( typeof( window.innerWidth ) == 'number' ) {
	    //Non-IE
	    myWidth = window.innerWidth;
	    myHeight = window.innerHeight;
	  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
	    //IE 6+ in 'standards compliant mode'
	    myWidth = document.documentElement.clientWidth;
	    myHeight = document.documentElement.clientHeight;
	  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
	    //IE 4 compatible
	    myWidth = document.body.clientWidth;
	    myHeight = document.body.clientHeight;
	  }
	  return [myWidth, myHeight];
	},
	verifyKeyCode: function(evt){
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode == 95 || charCode == 33 || charCode == 32 || charCode == 8){ return true; }
		if (charCode > 43 && charCode < 60){ return true; }
		if (charCode > 64 && charCode < 91){ return true; }
		if (charCode > 96 && charCode < 123){ return true; }
		return false;
	},
	isNumKey: function(evt){
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 47 && charCode < 58){ return true; }
		return false;
	},
	toPath: function(string){ return string.replace(/\\/g, "/");}
};

function $M(object) {
	var map = $H(object);
	Object.extend(map, Pyxis.HashExtensions);  
	return map;
};
Pyxis.HashExtensions =
{
	remove:function(key) 
	{ 
		try{ 
			delete this[key];
		}catch(e){} 
	},
	size:function() { return this.keys().length; },
	isEmpty:function() { return this.size() === 0; },
	clear:function() 
	{ 
		for (var key in this) 
		{ 
			var value = this[key]; 
			if (typeof value == 'function')
			{
				 continue; 
			} 
			try
			{
				delete this[key];
				value = null;
			}
			catch(e){alert(e)} 
		} 
	},
	eachEntry:function(iterator) { this.each(function(entry, index) { iterator(entry[0], entry[1]); }); },
	eachValue:function(iterator) { this.eachEntry(function(key, value) { if (typeof value != 'function'){iterator(value); } }); }
};