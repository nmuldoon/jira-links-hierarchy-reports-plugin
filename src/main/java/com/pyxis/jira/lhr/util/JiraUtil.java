package com.pyxis.jira.lhr.util;

/*
 * Copyright (c) 2007, Pyxis-Technologies inc.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import webwork.action.ActionContext;

import com.atlassian.core.user.preferences.Preferences;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.preferences.JiraUserPreferences;
import com.atlassian.jira.user.preferences.PreferenceKeys;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.seraph.auth.DefaultAuthenticator;
import com.opensymphony.user.User;

public class JiraUtil 
{
	/**
	 * @param issueKey
	 * @return the Issue of the giving key
	 */
    public static Issue getIssue(String issueKey)
    {
        try 
        {
			return ManagerFactory.getIssueManager().getIssueObject(issueKey);
		}
        catch (DataAccessException e) 
        {
        	return null;
		}
    }
    
    /**
     * @param issueKey
     * @return the Project id of the issue of the giving key
     */
    public static Long getProjectId(String issueKey)
    {
        return getProjectId(getIssue(issueKey));
    }
    
    /**
     * @param issue
     * @return the Project id of the issue
     */
    public static Long getProjectId(Issue issue)
    {
    	// The next alternative when this is no longer supported
    	// return (Long)issue.getProjectObject().getId();
        return (Long)issue.getProject().get("id");
    }

    /**
     * @param versions
     * @param projectId
     * @return all issue of a specific fixed version.
     * @throws SearchException
     */
    public static List getIssuesOfFixedVerions(Collection versions, Long projectId) throws SearchException
    {
        User remoteUser = (User) ActionContext.getSession().get(DefaultAuthenticator.LOGGED_IN_KEY);
        SearchProvider searchProvider = ComponentManager.getInstance().getSearchProvider();
        
        JqlClauseBuilder builder = JqlQueryBuilder.newClauseBuilder();
        builder.project(projectId).and().fixVersion(toParams(versions));
        SearchResults results = searchProvider.search(builder.buildQuery(), remoteUser, PagerFilter.getUnlimitedFilter());
        
        return results.getIssues();
    }

    /**
     * @param parentIssue
     * @param issue
     * @return true if issue is a subtask of the given potential parentIssue
     */
	public static boolean isASubTask(Issue parentIssue, Issue issue)
    {
		Iterator iter = parentIssue.getSubTaskObjects().iterator();
		while(iter.hasNext())
		{
			MutableIssue subtask = (MutableIssue)iter.next();
			if (issue.getKey().equals(subtask.getKey()))
				return true;
		}
		
		return false;
    }
    
    /**
     * Retreives the Locale of the user. 
     * @return the Locale of the user. If none found the default locale will then
     * be returned.
     */
    public static Locale getUserLocale()
    { 
        String locale = getUserPreferences().getString(PreferenceKeys.USER_LOCALE);
        if (!StringUtil.isEmpty(locale))
        {
            return ManagerFactory.getJiraLocaleUtils().getLocale(locale);
        }
        else
        {
            return ManagerFactory.getApplicationProperties().getDefaultLocale();
        }
    }
    
    /**
     * Retreives the preferences of the user.
     * @return the preferences of the user.
     */
    public static Preferences getUserPreferences()
    { 
        User remoteUser = (User) ActionContext.getSession().get(DefaultAuthenticator.LOGGED_IN_KEY);
        if (remoteUser == null)
        {
            return new JiraUserPreferences(null);
        }
        else
        {
            return new JiraUserPreferences(remoteUser);
        }
    }
    
    private static Long[] toParams(Collection<Version>versions)
    {
    	int i = 0;
    	Long[] versionIds = new Long[versions.size()];
    	for(Version version : versions)
    		versionIds[i] = version.getId();
    	
    	return versionIds;
    }
}
