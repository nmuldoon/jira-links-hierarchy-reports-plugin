package com.pyxis.jira.report.links;

/*
 * Copyright (c) 2007, Pyxis-Technologies inc.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.util.ParameterUtils;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.pyxis.jira.lhr.util.JiraUtil;
import com.pyxis.jira.report.actions.IssueHierarchyAction;
import com.pyxis.jira.report.services.HierarchyReportManager;

public class IssueHierarchyReport extends HierarchyReport
{  
    
    public IssueHierarchyReport(HierarchyReportManager hierarchyReportService, IssueLinkTypeManager linkTypeManager, IssueLinkManager issueLinkManager, SubTaskManager subTaskManager)
    {
        super(hierarchyReportService, linkTypeManager, issueLinkManager, subTaskManager);
    }

    protected String generateHtml(ProjectActionSupport action, Map parameters)
    {
        Map velocityParams = new HashMap();
        String issueKey = ParameterUtils.getStringParam(parameters, "issueKey").toUpperCase();
        IssueHierarchyAction issueAction = new IssueHierarchyAction(hierarchyReportService, issueLinkManager, subTaskManager);        

        Issue issue = JiraUtil.getIssue(issueKey);
        if(issue == null || issue.getId() == null)
        {
            return error("pyxis.hierarchy.report.noissues");
        }
        else
        {
            issueAction.setIssueKey(issueKey);
            issueAction.setLinkTypeId(((IssueLinkType)linkTypes.iterator().next()).getId());
            issueAction.setView(HIERARCHY_VIEW);
            issueAction.setThemeName(themeName);
            issueAction.setDecorator(decorator);
            issueAction.setTarget(true);
            issueAction.setOrphans(orphans);
            issueAction.setBulkUID("GRAPH_0");
            issueAction.setRetrieveChildren(true);
            issueAction.setRetrieveParents(true);
            issueAction.setSubtasks(subtasks);
            List issueCells = new ArrayList();
            issueCells.add(new IssueCell(issue, null, linkTypes, subtasks, ""));
            issueAction.setVersionId(new Long(-1l));
            issueAction.setIssueCells(issueCells);
        }

        velocityParams.put("action", issueAction);
        velocityParams.put("isTarget", Boolean.TRUE);
        velocityParams.put("isPrintView", Boolean.valueOf(decorator.equals("printable")));
        String html = descriptor.getHtml("view", velocityParams);
        
        return html;
    }
}