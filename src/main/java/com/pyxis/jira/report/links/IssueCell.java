package com.pyxis.jira.report.links;

/*
 * Copyright (c) 2007, Pyxis-Technologies inc.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.IssueUtilsBean;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.web.action.IssueActionSupport;
import com.pyxis.jira.lhr.util.StringUtil;

public class IssueCell
{
    public static final String SEPARATOR = "@";

    private IssueManager issueManager;
    private IssueLinkManager linkManager;
    
    private IssueActionSupport issueUtil;
    private Version version;
    private Issue issue;
    private Collection linkTypes;
    private boolean includeSubtasks;
    
    private List parents;    
    private List children;
    private Boolean hasParents;
    private Boolean hasChildren;
    private String sequence;
    private Integer treeIndent;


    public IssueCell(Issue issue, Version version, Collection linkTypes, boolean includeSubtasks, String sequence)
    {
        this.issueManager = ManagerFactory.getIssueManager();
        this.linkManager = ComponentManager.getInstance().getIssueLinkManager();
        this.issueUtil = new IssueActionSupport();
        this.issue = issue;
        this.version = version;
        
        this.includeSubtasks = includeSubtasks;
        this.linkTypes = linkTypes;
        this.sequence = sequence;
    }
    
    public Issue getIssue()
    {
        return issue;
    }
    
    public String getTimeSpent()
    {
        Long time = issue.getTimeSpent();
        return time == null ? "N/A" : issueUtil.getPrettyDuration(time);
    }
    
    public String getOriginalEstimate()
    {
        Long time = issue.getOriginalEstimate();
        return time == null ? "N/A" : issueUtil.getPrettyDuration(time);
    }
    
    public boolean isResolved()
    {
    	int status = Integer.valueOf(issue.getStatusObject().getId()).intValue();
    	return  status == IssueFieldConstants.RESOLVED_STATUS_ID || status == IssueFieldConstants.CLOSED_STATUS_ID;
    }
    
    /**
     * @return true if issue has parents based on the link type and if asked is a subtask.
     */
    public boolean hasParents()
    {
        if(hasParents != null) return hasParents.booleanValue();

        hasParents = Boolean.FALSE;
        Iterator iter = linkManager.getOutwardLinks(issue.getId()).iterator();
        while(iter.hasNext())
        {          
        	IssueLink link = (IssueLink)iter.next();
            if (isLinked(link))
            {             
                Issue issueParent = issueManager.getIssueObject(link.getDestinationId());
                if(isInSameVersion(issueParent))
                {
                    hasParents = Boolean.TRUE;
                    return hasParents.booleanValue();
                } 
            }
        }
        
        if(includeSubtasks)
        {
            Issue issueParent = issue.getParentObject();
            if(issueParent != null)
            {
                if(isInSameVersion(issueParent)) 
                {
                    hasParents = Boolean.TRUE;
                    return hasParents.booleanValue();
                } 
            }
        }
        
        return hasParents.booleanValue();
    }

    /**
     * @return The parents of the issue 
     */
    public List getParents()
    {
        if(parents != null) return parents;

        parents = new ArrayList();
        Iterator iter = linkManager.getOutwardLinks(issue.getId()).iterator();
        while(iter.hasNext())
        {          
        	IssueLink link = (IssueLink)iter.next();
            if (isLinked(link))
            {             
                Issue issueParent = issueManager.getIssueObject(link.getDestinationId());
                if(isInSameVersion(issueParent))
                    parents.add(new IssueCell(issueParent, version, linkTypes, includeSubtasks, sequence));                
            }
        }
        
        if(includeSubtasks)
        {
            Issue issueParent = issue.getParentObject();
            if(issueParent != null)
            {
            	IssueCell cell = new IssueCell(issueParent, version, linkTypes, includeSubtasks, sequence);
                if(!parents.contains(cell) && isInSameVersion(issueParent))
                    parents.add(cell);
            }
        }

        return parents;
    }
    
    /**
     * @return true if issue has children based on the link type and if asked has subtasks.
     */
    public boolean hasChildren()
    {
        if(hasChildren != null) return hasChildren.booleanValue();

        hasChildren = Boolean.FALSE;
        Iterator iter = linkManager.getInwardLinks(issue.getId()).iterator();
        while(iter.hasNext())
        {          
        	IssueLink link = (IssueLink)iter.next();
            if (isLinked(link))
            {             
                Issue issueChild = issueManager.getIssueObject(link.getSourceId());
                if(isInSameVersion(issueChild)) 
                {
                    hasChildren = Boolean.TRUE;
                    return hasChildren.booleanValue();
                }
            }
        }
        
        if(includeSubtasks)
        {
            Iterator iterSub = issue.getSubTaskObjects().iterator();
            while(iterSub.hasNext())
            {
            	Issue issueChild = (Issue)iterSub.next();
                if(isInSameVersion(issueChild))
                {
                    hasChildren = Boolean.TRUE;
                    return hasChildren.booleanValue();
                }
            }
        }

        return hasChildren.booleanValue();
    }


    /**
     * @return The children of the issue 
     */
    public List getChildren()
    {
        if(children != null) return children;

        children = new ArrayList();
        Iterator iter = linkManager.getInwardLinks(issue.getId()).iterator();
        while(iter.hasNext())
        {
        	IssueLink link = (IssueLink)iter.next();
            if (isLinked(link))
            {             
                Issue issueChild = issueManager.getIssueObject(link.getSourceId());
                if(isInSameVersion(issueChild)) 
                    children.add(new IssueCell(issueChild, version, linkTypes, includeSubtasks, sequence));
            }
        }
        
        if(includeSubtasks)
        {
        	Iterator iterSub = issue.getSubTaskObjects().iterator();
            while(iterSub.hasNext())
            {
            	Issue issueChild = (Issue)iterSub.next();
            	IssueCell cell = new IssueCell(issueChild, version, linkTypes, includeSubtasks, sequence);
                if(!children.contains(cell) && isInSameVersion(issueChild)) 
                    children.add(cell);
            }
        }

        return children;
    }
    
    public String getSequence()
    {
        return StringUtil.isEmpty(sequence) ? issue.getKey() : sequence + SEPARATOR + issue.getKey();
    }

    public Integer getTreeIndent()
    {
        if(treeIndent != null) return treeIndent;
        StringTokenizer stk = new StringTokenizer(sequence, IssueCell.SEPARATOR);
        treeIndent = new Integer(stk.countTokens());
        return treeIndent;
    }
    
    public List getIndents()
    {
        List indents = new ArrayList();
        if(StringUtil.isEmpty(sequence)) return indents;
        
        Iterator iter = Arrays.asList(sequence.split(IssueCell.SEPARATOR)).iterator();
        while(iter.hasNext())
        {
        	String indent = (String)iter.next();
            indents.add(indent);
        }
        
        return indents;
    }

    public void setTreeIndent(Integer treeIndent)
    {
        this.treeIndent = treeIndent;
    }
    
    public boolean isTopLevel()
    {
        return !hasParents();
    }
    
    public boolean isOrphan()
    {
        return !hasParents() && !hasChildren();
    }
    
    public boolean isRecursive()
    {
        if(StringUtil.isEmpty(sequence)) return false;
        StringTokenizer stk = new StringTokenizer(sequence, SEPARATOR);
        while(stk.hasMoreTokens())
        {
            if(stk.nextToken().equals(issue.getKey()))
            {
                return true;
            }
        }

        return false;
    }
	
	public Map getWorkflowActions()
	{
    	return ((IssueUtilsBean)ComponentManager.getComponentInstanceOfType(IssueUtilsBean.class)).loadAvailableActions(issue);
	}
	
	public boolean equals(Object o)
	{
		if(o == null) return false;
		if(!(o instanceof IssueCell)) return false;
		
		return getSequence().equals(((IssueCell)o).getSequence());
	}
	
	public int hashCode()
	{
		return getSequence().hashCode();
	}

    private boolean isInSameVersion(Issue issue)
    {
        if(version == null) return true;
        Collection versions = issue.getFixVersions();
        return versions.contains(version);
    }

    /**
     * For now the logic is based on a single IssueLink we should in a near future
     * support multi links.
     * @param link
     * @return true if Linked
     */
    private boolean isLinked(IssueLink link)
    {
        return linkTypes.contains(link.getIssueLinkType());
    }
}
