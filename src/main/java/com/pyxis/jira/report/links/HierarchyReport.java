package com.pyxis.jira.report.links;

/*
 * Copyright (c) 2007, Pyxis-Technologies inc.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.plugin.report.impl.AbstractReport;
import com.atlassian.jira.util.ParameterUtils;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.pyxis.jira.lhr.util.StringUtil;
import com.pyxis.jira.report.actions.IssueHierarchyAction;
import com.pyxis.jira.report.services.HierarchyReportManager;

public abstract class HierarchyReport extends AbstractReport
{
    public static final String ORGANOGRAM = "org";
    public static final String TREE = "tree";
    public static final String ISSUE_VIEW = "issueView";
    public static final String HIERARCHY_VIEW = "hierarchyView";
    
    protected IssueLinkTypeManager linkTypeManager;
    protected HierarchyReportManager hierarchyReportService;
    protected IssueLinkManager issueLinkManager;
    protected SubTaskManager subTaskManager;
    
    protected Collection  linkTypes;
    protected boolean subtasks;
    protected String themeName;
    protected boolean isTreeView;
    protected boolean orphans;
    protected String decorator;
    
    public HierarchyReport(HierarchyReportManager hierarchyReportService, IssueLinkTypeManager linkTypeManager, IssueLinkManager issueLinkManager, SubTaskManager subTaskManager)
    {
        this.linkTypeManager = linkTypeManager;
        this.hierarchyReportService = hierarchyReportService;
        this.issueLinkManager = issueLinkManager;
        this.subTaskManager = subTaskManager;
    }

    public String generateReportHtml(ProjectActionSupport action, Map parameters)
    {
    	linkTypes = new ArrayList(); 
        Long linkTypeId = ParameterUtils.getLongParam(parameters, "linkTypeId");
        linkTypes.add(linkTypeManager.getIssueLinkType(linkTypeId));
        
        String subtasksParam = ParameterUtils.getStringParam(parameters, "subtasks");
        subtasks = subtasksParam != null ? Boolean.valueOf(subtasksParam).booleanValue() : false;
        
        themeName = ParameterUtils.getStringParam(parameters, "themeName");
        
        String view = ParameterUtils.getStringParam(parameters, HIERARCHY_VIEW);
        isTreeView = !StringUtil.isEmpty(view) ? view.equals(TREE): false;
        
        String deco = ParameterUtils.getStringParam(parameters, "decorator");
        decorator = !StringUtil.isEmpty(deco) ? deco: "none"; 
        
        String orph = ParameterUtils.getStringParam(parameters, "orphans");
        orphans = orph != null ? Boolean.valueOf(orph).booleanValue() : false;
        
        String html = generateHtml(action, parameters);
        return html;
    }
    
    public String error(String errorId)
    {
        Map velocityParams = new HashMap();
        velocityParams.put("error", errorId);
        velocityParams.put("action", new IssueHierarchyAction(hierarchyReportService, issueLinkManager, subTaskManager));
        return descriptor.getHtml("view", velocityParams);
    }
    
    abstract protected String generateHtml(ProjectActionSupport action, Map parameters);
}