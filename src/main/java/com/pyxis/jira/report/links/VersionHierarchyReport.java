package com.pyxis.jira.report.links;

/*
 * Copyright (c) 2007, Pyxis-Technologies inc.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.util.ParameterUtils;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.opensymphony.user.User;
import com.pyxis.jira.lhr.util.JiraUtil;
import com.pyxis.jira.report.actions.IssueHierarchyAction;
import com.pyxis.jira.report.services.HierarchyReportManager;

public class VersionHierarchyReport extends HierarchyReport
{      
    private VersionManager versionManager;
    
    public VersionHierarchyReport(HierarchyReportManager hierarchyReportService, VersionManager versionManager, IssueLinkTypeManager linkTypeManager, IssueLinkManager issueLinkManager, SubTaskManager subTaskManager)
    {
        super(hierarchyReportService, linkTypeManager, issueLinkManager, subTaskManager);
        this.versionManager = versionManager;
    }

    public String generateHtml(ProjectActionSupport action, Map parameters)
    {
        Map velocityParams = new HashMap();
        Version version = versionManager.getVersion(ParameterUtils.getLongParam(parameters, "versionId"));
        if(version == null ) return error("pyxis.hierarchy.report.noversion");
        
        Long projectId = ParameterUtils.getLongParam(parameters, "selectedProjectId");
        User user = action.getRemoteUser();
        
        List cells = getTopLevelCells(projectId, version, orphans, user);
        if(cells.isEmpty()) return error("pyxis.hierarchy.report.noversionissues");
        
        int index = 0;
        List issueActions = new ArrayList();
        Iterator iter = cells.iterator();
        while(iter.hasNext())
        {
        	IssueCell cell = (IssueCell)iter.next();
            IssueHierarchyAction issueAction = new IssueHierarchyAction(hierarchyReportService, issueLinkManager, subTaskManager);        
            issueAction.setIssueKey(cell.getIssue().getKey());
            
            //@TODO Support only 1 link as for now
            issueAction.setLinkTypeId(((IssueLinkType)linkTypes.iterator().next()).getId());
            issueAction.setView(HIERARCHY_VIEW);

            issueAction.setRetrieveChildren(true);
            issueAction.setRetrieveParents(false);
            issueAction.setSubtasks(subtasks);
            issueAction.setVersionId(version.getId());
            issueAction.setBulkUID("GRAPH_" + index++);
            issueAction.setTreeView(isTreeView);
            issueAction.setOrphans(orphans);
            issueAction.setThemeName(themeName);
            issueAction.setDecorator(decorator);
            issueAction.setTarget(true);
            issueActions.add(issueAction);
            List issueCells = new ArrayList();
            issueAction.setIssueCells(issueCells);
            issueCells.add(cell);
        }

        velocityParams.put("issueActions", issueActions);
        velocityParams.put("isPrintView", Boolean.valueOf(decorator.equals("printable")));
        velocityParams.put("isTarget", Boolean.TRUE);
        String html = descriptor.getHtml("view", velocityParams);
        
        return html;
    }

    private List getTopLevelCells(Long projectId, Version version, boolean showOrphans, User user)
    {
        List topLevelCells = new ArrayList();
        
        try
        {
            List versions = new ArrayList(); versions.add(version);
            Iterator iter = JiraUtil.getIssuesOfFixedVerions(versions, projectId).iterator();
            while(iter.hasNext())
            {
            	Issue issue = (Issue)iter.next();
                IssueCell cell = new IssueCell(JiraUtil.getIssue(issue.getKey()), version, linkTypes, subtasks, "");
                if(cell.isTopLevel())
                {
                    if(showOrphans)
                    {
                        topLevelCells.add(cell);
                    }
                    else if(!cell.isOrphan())
                    {
                        topLevelCells.add(cell);
                    }
                }
            }
        }        
        catch (SearchException e){}
        
        return topLevelCells;
    }
}