package com.pyxis.jira.report.actions;

/*
 * Copyright (c) 2007, Pyxis-Technologies inc.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ofbiz.core.entity.GenericValue;

import webwork.action.ActionContext;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.seraph.auth.DefaultAuthenticator;
import com.opensymphony.user.User;
import com.pyxis.jira.actions.AbstractPyxisAction;
import com.pyxis.jira.report.links.HierarchyReport;
import com.pyxis.jira.report.links.IssueCell;
import com.pyxis.jira.report.services.HierarchyReportManager;


/**
 * This class will provide support for the remote confluence plugin.
 * Still at a Prototype level
 * 
 * @author jchuet
 */
public class ConfluenceActionSupport extends AbstractPyxisAction
{
    private static final String ALL_VERSIONS = "ALL";

    private IssueLinkTypeManager linkTypeManager;
    private HierarchyReportManager hierarchyReportService;

    private Set issueActions;  

    private GenericValue project;
    private String projectName;
    private String themeName;
    private String versionName;
    private String linkTypeName;
    private Version version;
    private Map selections;
    
    public ConfluenceActionSupport(HierarchyReportManager hierarchyReportService, IssueLinkTypeManager linkTypeManager, IssueLinkManager issueLinkManager, SubTaskManager subTaskManager)
    {
    	super(issueLinkManager, subTaskManager);
        this.hierarchyReportService = hierarchyReportService;
        this.linkTypeManager = linkTypeManager;
    }
    
    public String doVersionHierarchy()
    {
        try
        {
            validateHierarchyQuery();
            if(super.hasAnyErrors()) return SUCCESS;
            
            int index = 0;
            issueActions = new HashSet();
            Iterator iter = getTopLevelCells().iterator();
            while(iter.hasNext())
            {
            	IssueCell cell = (IssueCell)iter.next();
                IssueHierarchyAction issueAction = new IssueHierarchyAction(hierarchyReportService, getIssueLinkManager(), getSubTaskManager());        
                issueAction.setIssueKey(cell.getIssue().getKey());
                issueAction.setLinkTypeId(getLinkType().getId());
                issueAction.setView(HierarchyReport.HIERARCHY_VIEW);

                issueAction.setRetrieveChildren(true);
                issueAction.setRetrieveParents(false);
                issueAction.setSubtasks(true);
                issueAction.setVersionId(getVersion().getId());
                issueAction.setBulkUID("GRAPH_" + versionName.replaceAll(" ", "_") + index++);
                issueAction.setTreeView(false);
                issueAction.setThemeName(themeName);
                issueAction.setTarget(true);
                issueActions.add(issueAction);
                
                List issueCells = new ArrayList();
                issueCells.add(cell);
                issueAction.setIssueCells(issueCells);
            }
        }
        catch (SearchException e)
        {
            addError(e.getMessage(), e.getMessage());
        }
        
        return SUCCESS;
    }

    public Set getIssueActions()
    {
        return issueActions;
    }
    
    public String getProjectName()
    {
        return projectName;
    }
    
    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }
    
    public String getThemeName()
    {
        return themeName;
    }
    
    public void setThemeName(String themeName)
    {
        this.themeName = themeName;
    }
    
    public String getVersionName()
    {
        return versionName;
    }
    
    public void setVersionName(String versionName)
    {
        this.versionName = versionName;
    }

    public String getLinkTypeName()
    {
        return linkTypeName;
    }

    public void setLinkTypeName(String linkTypeName)
    {
        this.linkTypeName = linkTypeName;
    }
    
    public Long getProjectId()
    {
        return getProject().getLong("id");
    }
    
    public GenericValue getProject()
    {
        if(project != null) { return project; }
        project = projectManager.getProjectByName(getProjectName());
        return project;
    }
    
    public boolean getIsforAllVersions()
    {
        return versionName.equalsIgnoreCase(ALL_VERSIONS);
    }
    
    public boolean getBulkView()
    {
        return true;
    }
    
    public List getVersions()
    { 
        List versions = new ArrayList();
        if(getIsforAllVersions())
        {
            versions = getVersionManager().getVersions(getProject());
        }
        else
        {
            versions.add(getVersion());
        }    
          
        return versions;
    }
    
    public IssueLinkType getLinkType()
    {
        Collection linkTypes = linkTypeManager.getIssueLinkTypesByName(linkTypeName);
        if(linkTypes.isEmpty()) return null;
        return (IssueLinkType)linkTypes.iterator().next();
    }
    
    public Map getAllProjectAndVersions()
    {
        if(selections != null){ return selections; }
        Map selections = new HashMap();
        Collection projects = projectManager.getProjects();
        Iterator iter = projects.iterator();
        while(iter.hasNext())
        {
            GenericValue project = (GenericValue)iter.next();
            List versions = getVersionManager().getVersions(project);
            selections.put((String)project.get("name"), versions);
        }
        
        return selections;
    }
    
    public Collection getLinkTypes()
    {
        return linkTypeManager.getIssueLinkTypes();
    }
    
    public void validateHierarchyQuery()
    {
        if(getProject() == null)
            addError("greenpepper.bulkresult.invalidproject", "invalid project");

        if(getVersion() == null)
            addError("greenpepper.bulkresult.invalidversion", "invalid version");

        if(getLinkType() == null)
        	addError("greenpepper.bulkresult.invalidlinktype", "invalid Link Type");


        if(!versionName.equalsIgnoreCase("ALL"))
            if(getVersion() == null)
            	addError("greenpepper.bulkresult.invalidversion", "invalid version");
    }
    
    private Version getVersion()
    {
        if(version != null){ return version; }
        Version version = getVersionManager().getVersion(getProjectId(), versionName);
        return version;
    }

    private List getTopLevelCells() throws SearchException
    {
        List  topLevelCells = new ArrayList();        
        List versions = new ArrayList(); 
        versions.add(getVersion());
        Iterator iter = getIssuesOfFixedVerions(versions, getProjectId()).iterator();
        while(iter.hasNext())
        {
        	Issue issue = (Issue)iter.next();
            IssueCell cell = new IssueCell(issue, version, getLinkTypes(), true, "");
            if(cell.isTopLevel())
            {
                topLevelCells.add(cell);
            }
        }    
        return topLevelCells;
    }

    public static List getIssuesOfFixedVerions(Collection versions, Long projectId) throws SearchException
    {
        User remoteUser = (User) ActionContext.getSession().get(DefaultAuthenticator.LOGGED_IN_KEY);
        SearchProvider searchProvider = ComponentManager.getInstance().getSearchProvider();

        JqlClauseBuilder builder = JqlQueryBuilder.newClauseBuilder();
        builder.project(projectId).and().fixVersion(toParams(versions));
        SearchResults results = searchProvider.search(builder.buildQuery(), remoteUser, PagerFilter.getUnlimitedFilter());
        
        return results.getIssues();
    }
    
    private static Long[] toParams(Collection<Version>versions)
    {
    	int i = 0;
    	Long[] versionIds = new Long[versions.size()];
    	for(Version version : versions)
    		versionIds[i] = version.getId();
    	
    	return versionIds;
    }
}
