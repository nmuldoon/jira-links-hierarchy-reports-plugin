package com.pyxis.jira.report.actions;

/*
 * Copyright (c) 2007, Pyxis-Technologies inc.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.Permissions;
import com.pyxis.jira.actions.AbstractPyxisAction;
import com.pyxis.jira.report.links.HierarchyReport;
import com.pyxis.jira.report.links.IssueCell;
import com.pyxis.jira.report.links.themes.IssueCellTheme;
import com.pyxis.jira.report.links.themes.WorkflowTheme;
import com.pyxis.jira.report.services.HierarchyReportManager;

/**
 * Issue Hierarchy Action. Basically will be avle to retrieve the parents and the 
 * children of an issue based on a linktype.
 * 
 * @author jchuet
 */
public class IssueHierarchyAction extends AbstractPyxisAction
{
	private HierarchyReportManager hierarchyReportService;
	
    private List issueCells = new ArrayList();
    private Long versionId;

    private IssueCell issueCell;
    private String sequence = "";
    private boolean retrieveParents;
    private boolean retrieveChildren;
    
    private Long linkTypeId;
    private boolean subtasks;
    private boolean isTreeView;
    private boolean target;
    private IssueCellTheme theme;
    private String themeName;
    private boolean orphans = true;
    private Boolean canLink;
    
    public IssueHierarchyAction(HierarchyReportManager hierarchyReportService, IssueLinkManager issueLinkManager, SubTaskManager subTaskManager)
    {
    	super(issueLinkManager, subTaskManager);
    }
    
    public String doGetIssueCell()
    {
    	if(!hasErrors())
    	{
	    	target = true;
	        issueCell = new IssueCell(getIssueObject(), getVersion(), getLinkTypes(), subtasks, "");
	        retrieveParents = issueCell.hasParents();
	        retrieveChildren = true;
	        issueCells.add(issueCell);
    	}
        
        return isTreeView ? "treeView" : SUCCESS;
    }

    public String doGetParents()
    {
    	if(!hasErrors())
    	{
	        retrieveParents = true;
	        retrieveChildren = false;
	        setView(HierarchyReport.HIERARCHY_VIEW);
	        issueCells = getIssueCell().getParents();
    	}

        return isTreeView ? "treeView" : SUCCESS;
    }

    public String doGetChildren()
    {
    	if(!hasErrors())
    	{
	        retrieveParents = false;
	        retrieveChildren = true;
	        setView(HierarchyReport.HIERARCHY_VIEW);
	        issueCells = getIssueCell().getChildren();
    	}

        return isTreeView ? "treeView" : SUCCESS;
    }
    
/***************************************************************************************/
    

    public List getIssueCells()
    {
        return issueCells;
    }

    public void setIssueCells(List issueCells)
    {
        this.issueCells = issueCells;
    }
    
    public IssueCell getIssueCell()
    {
        if(issueCell != null) return issueCell;
        issueCell = new IssueCell(getIssueObject(), getVersion(), getLinkTypes(), subtasks, sequence);
        return issueCell;
    }

    public boolean getRetrieveParents()
    {
        return retrieveParents;
    }

    public void setRetrieveParents(boolean retrieveParents)
    {
        this.retrieveParents = retrieveParents;
    }

    public boolean getRetrieveChildren()
    {
        return retrieveChildren;
    }

    public void setRetrieveChildren(boolean retrieveChildren)
    {
        this.retrieveChildren = retrieveChildren;
    }

    public void setSequence(String sequence)
    {
        this.sequence = sequence;
    }

    public String getSequence()
    {
    	return sequence;
    }

    public Long getVersionId()
    {
        return versionId;
    }

    public void setVersionId(Long versionId)
    {
        this.versionId = versionId;
    }
    
    public Version getVersion()
    {
        if(versionId == null || versionId.longValue() == 1l) return null;
        VersionManager versionManager = (VersionManager)ComponentManager.getComponentInstanceOfType(VersionManager.class);
        return versionManager.getVersion(versionId);
    }
    
    public Long getLinkTypeId()
    {
        return linkTypeId;
    }
    
    public void setLinkTypeId(Long linkTypeId)
    {
        this.linkTypeId = linkTypeId;
    }
    
    public IssueLinkType getLinkType()
    {
        if(linkTypeId == null) return null;
        IssueLinkTypeManager linkTypeManager = (IssueLinkTypeManager)ComponentManager.getComponentInstanceOfType(IssueLinkTypeManager.class);
        return linkTypeManager.getIssueLinkType(linkTypeId);
    }
    
    /**
     * As for now we only support 1 link
     * @TODO multiple links
     * @return
     */
    public Collection getLinkTypes()
    {
    	Collection linkTypes = new ArrayList();
    	linkTypes.add(getLinkType());
        return linkTypes;
    }

    public boolean getSubtasks()
    {
        return subtasks;
    }

    public void setSubtasks(boolean subtasks)
    {
        this.subtasks = subtasks;
    }

    public boolean isTreeView()
    {
        return isTreeView;
    }

    public void setTreeView(boolean isTreeView)
    {
        this.isTreeView = isTreeView;
    }

	public String getThemeName() 
	{
        if(themeName == null)
            themeName = "Default";
        
		return themeName;
	}

	public void setThemeName(String themeName)
	{
		this.themeName = themeName;
	}
	
	public IssueCellTheme getTheme()
	{
		if(theme != null) return theme;
		
		try 
		{
			Class themeClass = Class.forName((String)hierarchyReportService.getRegisteredThemes().get(themeName));
			theme = (IssueCellTheme)themeClass.getConstructor(new Class[]{this.getClass()}).newInstance(new Object[]{this});
		} 
		catch (Exception e) 
		{
			theme = new WorkflowTheme(this);
		}
		
		return theme;
	}

	public boolean isTarget() 
	{
		return target;
	}

	public void setTarget(boolean target)
	{
		this.target = target;
	}

	public boolean isOrphans() 
	{
		return orphans;
	}

	public void setOrphans(boolean orphans) 
	{
		this.orphans = orphans;
	}
	
	public boolean getCanLink()
	{
		if(canLink != null) return canLink.booleanValue();
		canLink = new Boolean(isHasIssuePermission(Permissions.LINK_ISSUE, getIssue()));
		return canLink.booleanValue();
	}
}
