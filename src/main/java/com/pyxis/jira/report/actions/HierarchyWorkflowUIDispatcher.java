package com.pyxis.jira.report.actions;

/*
 * Copyright (c) 2007, Pyxis-Technologies inc.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.util.Iterator;
import java.util.Map;

import webwork.action.ActionContext;

import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.web.action.workflow.WorkflowAwareAction;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowTransitionUtil;
import com.atlassian.jira.workflow.WorkflowTransitionUtilImpl;
import com.opensymphony.util.TextUtils;
import com.opensymphony.workflow.Workflow;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import com.pyxis.jira.report.services.HierarchyReportManager;

/**
 * This action was ment to replace Jira s default WorkflowUIDispatcher so we can control
 * the redirection for Ajax support. It is pretty much a merge btween the SimpleWorkflowAction
 * and the simpleWorkflowAction.
 * 
 * @author jchuet
 */
public class HierarchyWorkflowUIDispatcher extends IssueHierarchyAction implements WorkflowAwareAction
{
    private final WorkflowManager workflowManager;
    private int action;
    private boolean needsInputs;

    public HierarchyWorkflowUIDispatcher(HierarchyReportManager hierarchyReportService, IssueLinkManager issueLinkManager, SubTaskManager subTaskManager, WorkflowManager workflowManager)
    {
        super(hierarchyReportService, issueLinkManager, subTaskManager);
        this.workflowManager = workflowManager;
    }

    public String doExecute() throws Exception
    {
    	if(!hasErrors())
    	{
	        if (TextUtils.stringSet(getActionDescriptor().getView()))
	        {
	        	needsInputs = true;
	        }
	        else
	        {
	            simpleWorkflowAction();
	        }
    	}
    	
        return SUCCESS;
    }

    public String getWorkflowTransitionDisplayName()
    {
        return getWorkflowTransitionDisplayName(getActionDescriptor());
    }
    
    public int getAction()
    {
        return action;
    }

    public void setAction(int action)
    {
        this.action = action;
    }
    
    private void simpleWorkflowAction()
    {
        WorkflowTransitionUtil workflowTransitionUtil = (WorkflowTransitionUtil) JiraUtils.loadComponent(WorkflowTransitionUtilImpl.class);
        workflowTransitionUtil.setIssue(getIssueObject());
        workflowTransitionUtil.setAction(getAction());

        addErrorCollection(workflowTransitionUtil.progress());
        if (invalidInput())
            addError("Invalid inputs", getText("linkissue.error.invalidworkflowinputs"));
    }

    private ActionDescriptor getActionDescriptor()
    {
        String username = (getRemoteUser() != null ? getRemoteUser().getName() : null);
        Workflow wf = workflowManager.makeWorkflow(username);
        long workflowId = getIssue().getLong("workflowId").longValue();
        WorkflowDescriptor wd = wf.getWorkflowDescriptor(wf.getWorkflowName(workflowId));
        final ActionDescriptor actionDescriptor = wd.getAction(action);
        return actionDescriptor;
    }
    
    public String getCommentAssignIssuURL()
    {
        final StringBuffer url = new StringBuffer("CommentAssignIssue!default.jspa");
        String separator = "?";

        Map existingParams = ActionContext.getParameters();
        for (Iterator iterator = existingParams.keySet().iterator(); iterator.hasNext();)
        {
            String paramName = (String) iterator.next();
            String[] paramValues = (String[]) existingParams.get(paramName);
            for (int i = 0; i < paramValues.length; i++)
            {
                String paramValue = paramValues[i];
                url.append(separator).append(paramName).append("=").append(paramValue);
                separator = "&";
            }
        }
        
        return url.toString();
    }

	public boolean getNeedsInputs() 
	{
		return needsInputs;
	}
	
	public boolean hasErrors()
	{
        if (!issueUtilsBean.isValidAction(getIssueObject(), action))
            addError("Invalid workflow action", getText("linkissue.error.invalidworkflowaction"));
        
        return super.hasErrors();
	}
}
