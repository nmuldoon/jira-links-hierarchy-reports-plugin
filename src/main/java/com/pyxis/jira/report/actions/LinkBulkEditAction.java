package com.pyxis.jira.report.actions;

/*
 * Copyright (c) 2007, Pyxis-Technologies inc.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import webwork.action.ActionContext;

import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.web.SessionKeys;
import com.atlassian.jira.web.bean.BulkEditBean;
import com.pyxis.jira.report.links.IssueCell;
import com.pyxis.jira.report.services.HierarchyReportManager;

/**
 * This class is meant to by pass the SearchRequestAction. It will basically fill the bulkEdit issue
 * collection before calling the bulk edit page. 
 * 
 * @author jchuet
 */
public class LinkBulkEditAction extends IssueHierarchyAction
{
    private IssueManager issueManager;
	

    public LinkBulkEditAction(HierarchyReportManager hierarchyReportService, IssueManager issueManager, IssueLinkManager issueLinkManager, SubTaskManager subTaskManager)
    {
    	super(hierarchyReportService, issueLinkManager, subTaskManager);
        this.issueManager = issueManager;
    }
    
	public String doExecute() throws Exception
    {
    	initialiseBulkEditBean();
        ActionContext.getSession().put(SessionKeys.SEARCH_REQUEST, new SearchRequest());
        return getRedirect("BulkEdit1!default.jspa?reset=false");
    }

    private void initialiseBulkEditBean()
    {
        BulkEditBean bulkEditBean = new BulkEditBean(issueManager);
        Set issues = new HashSet();
        fillIssues(getIssueObject(), "", issues, true, true);
        bulkEditBean.setIssuesFromSearchRequest(new ArrayList(issues));
        bulkEditBean.setCurrentStep(1);
        
        BulkEditBean.storeToSession(bulkEditBean);
    }
    
    private void fillIssues(Issue issue, String sequence, Set issues, boolean retrieveParents, boolean retrieveChildren)
    {
        issues.add(issue);
        IssueCell issueCell = new IssueCell(issue, getVersion(), getLinkTypes(), getSubtasks(), sequence);
        
        if(retrieveParents)
        {
        	Iterator iter = issueCell.getParents().iterator();
        	while(iter.hasNext())
        	{
        		IssueCell parent = (IssueCell)iter.next();
        		if(!parent.isRecursive())
        			fillIssues(parent.getIssue(), issueCell.getSequence(), issues, true, false);
        	}
        }	
        if(retrieveChildren)
        {
        	Iterator iter = issueCell.getChildren().iterator();
        	while(iter.hasNext())
        	{
        		IssueCell child = (IssueCell)iter.next();
        		if(!child.isRecursive())
        			fillIssues(child.getIssue(), issueCell.getSequence(), issues, false, true);
        	}
        }
    }
}
