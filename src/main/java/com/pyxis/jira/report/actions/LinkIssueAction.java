package com.pyxis.jira.report.actions;

/*
 * Copyright (c) 2007, Pyxis-Technologies inc.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.web.action.issue.AbstractCommentableIssue;
import com.pyxis.jira.actions.AbstractPyxisAction;
import com.pyxis.jira.lhr.util.I18nUtil;
import com.pyxis.jira.lhr.util.JiraUtil;
import com.pyxis.jira.lhr.util.StringUtil;
import com.pyxis.jira.report.links.IssueCell;

/**
 * Would have loved to extend LinkExistingIssue but the excute method redirects the response =(
 * Class mapped on a 3.7 Jira
 */
public class LinkIssueAction extends AbstractCommentableIssue implements OperationContext
{
    private static final String RESOURCE_BUNDLE = AbstractPyxisAction.class.getName();
    private ResourceBundle resourceBundle;
    
	private String linkKey;
    private String linkDesc;
    private String issueKey;
    private List linkDescs;
    private String sequence;
    private String bulkUID;
    private String linkedIssues;

    private IssueLinkType issueLinkType;
    private IssueLinkTypeManager issueLinkTypeManager;

    public LinkIssueAction(IssueLinkManager issueLinkManager, IssueLinkTypeManager issueLinkTypeManager, SubTaskManager subTaskManager, FieldManager fieldManager, FieldScreenRendererFactory fieldScreenRendererFactory, ProjectRoleManager projectRoleManager, CommentService commentService)
    {
        super(issueLinkManager, subTaskManager, fieldScreenRendererFactory, fieldManager, projectRoleManager, commentService);
        this.issueLinkTypeManager = issueLinkTypeManager;
    }

	public String doAddLinks()
    {
		if(!hasAddErrors())
		{
	        IssueLinkType linkType = getIssueLinkType();
	        boolean addLocalComment = true;
	        Iterator iter = getKeys(linkKey).iterator();
	    	while(iter.hasNext())
	        {
	    		String key = (String)iter.next();
	            MutableIssue destinationIssue = ManagerFactory.getIssueManager().getIssueObject(key);
	            linkIssue(linkType, destinationIssue, addLocalComment);
	            addLocalComment = false;
	        }
		}

    	return SUCCESS;
    }
	
	public String doBreakLinks()
	{
		if(!hasBreakErrors())
		{
	        try
	        {
	
	        	MutableIssue issue = ManagerFactory.getIssueManager().getIssueObject(issueKey);
	            IssueLink issueLink = null;

		        Iterator iter = getKeys(linkKey).iterator();
		    	while(iter.hasNext())
	        	{
		    		String key = (String)iter.next();
		            MutableIssue linkedIssue = ManagerFactory.getIssueManager().getIssueObject(key);
		        	if(linkDesc.equals(getIssueLinkType().getOutward()))
		        	{
		        		issueLink = getIssueLinkManager().getIssueLink(issue.getId(), linkedIssue.getId(), getIssueLinkType().getId());
		        	}
		        	else
		        	{
			            issueLink = getIssueLinkManager().getIssueLink(linkedIssue.getId(), issue.getId(), getIssueLinkType().getId());
		        	}
	
		        	if(issueLink == null)
		        	{
			            addError("Break link failed", getText("linkissue.error.breakfailed"));
		        	}
		        	else
		        	{
		        		getIssueLinkManager().removeIssueLink(issueLink, getRemoteUser());
		        	}
	        	}
	        }
	        catch (Exception e)
	        {
	            addError("Break link failed", getText("linkissue.error.breakfailed"));
	        }
		}
		
    	return SUCCESS;
	}
    
    public String getIssueKey()
    {
    	return issueKey;
    }
    
    public void setIssueKey(String issueKey)
    {
    	this.issueKey = issueKey;
    	super.setKey(issueKey);
    }

    public String getLinkKey()
    {
        return linkKey;
    }

    public void setLinkKey(String linkKey)
    {
        this.linkKey = linkKey.toUpperCase().trim();
    }

    public String getLinkDesc()
    {
        return linkDesc;
    }

    public void setLinkDesc(String linkDesc)
    {
        this.linkDesc = linkDesc;
    }

    public Collection getLinkDescs()
    {
        if (linkDescs == null)
        {
            Collection linkTypes = issueLinkTypeManager.getIssueLinkTypes();

            linkDescs = new ArrayList();
            for (Iterator iterator = linkTypes.iterator(); iterator.hasNext();)
            {
                IssueLinkType issueLinkType = (IssueLinkType) iterator.next();
                linkDescs.add(issueLinkType.getOutward());
                linkDescs.add(issueLinkType.getInward());
            }
        }

        return linkDescs;
    }

	public String getBulkUID() 
	{
		return bulkUID;
	}

	public void setBulkUID(String bulkUID)
	{
		this.bulkUID = bulkUID;
	}

	public String getSequence() 
	{
		return sequence;
	}

	public void setSequence(String sequence)
	{
		this.sequence = sequence;
	}

	public String getLinkedIssues()
	{
		return linkedIssues;
	}

	public void setLinkedIssues(String linkedIssues) 
	{
		this.linkedIssues = linkedIssues;
	}
    
    public boolean isRecursive(String newIssueKey)
    {
        if(StringUtil.isEmpty(sequence)) return false;
        StringTokenizer stk = new StringTokenizer(sequence, IssueCell.SEPARATOR);
        
        while(stk.hasMoreTokens())
            if(stk.nextToken().equals(newIssueKey))
                return true;
        
        return false;
    }
    
    /**
     * Custom I18n. Based on WebWork i18n.
     * @param key
     * @return the i18nzed message. If none found key is returned.
     */
    public String getText(String key)
    {
        if (resourceBundle == null)
        {
            Locale locale = JiraUtil.getUserLocale();
            resourceBundle = ResourceBundle.getBundle(RESOURCE_BUNDLE, locale);
        }
        
        return I18nUtil.getText(key, resourceBundle);
    }

    private void linkIssue(IssueLinkType linkType, MutableIssue destinationIssue, boolean addCommentToLocalIssue)
    {
        try
        {
            if (linkDesc.equals(linkType.getOutward()))
                getIssueLinkManager().createIssueLink(getIssue().getLong("id"), destinationIssue.getId(), linkType.getId(), null, getRemoteUser());
            else
                getIssueLinkManager().createIssueLink(destinationIssue.getId(), getIssue().getLong("id"), linkType.getId(), null, getRemoteUser());

            if (addCommentToLocalIssue)
                createComment(getIssueObject());

            createComment(destinationIssue);
            getIssueObject().resetModifiedFields();
        }
        catch (Exception e)
        {
            addErrorMessage(getText("admin.errors.issues.an.error.occured", e));
        }
    }

    private IssueLinkType getIssueLinkType()
    {
        if (issueLinkType == null)
        {
            Collection linkTypes = issueLinkTypeManager.getIssueLinkTypes();
            for (Iterator iterator = linkTypes.iterator(); iterator.hasNext();)
            {
                IssueLinkType linkType = (IssueLinkType) iterator.next();
                if (linkDesc.equals(linkType.getOutward()) || linkDesc.equals(linkType.getInward()))
                {
                    issueLinkType = linkType;
                    break;
                }
            }
        }

        return issueLinkType;
    }

	private Collection getKeys(String keys)
    {
        if (StringUtil.isBlank(keys))
            return Collections.EMPTY_LIST;

        Collection keyCol = new ArrayList();
        StringTokenizer tokenizer = new StringTokenizer(keys, ", \t\n\r\f", false);
        while (tokenizer.hasMoreTokens())
            keyCol.add(tokenizer.nextToken());
        
        return keyCol;
    }

    private boolean hasAddErrors()
    {
    	if(hasGeneralErrors()) return true;
        Iterator iter = getKeys(linkKey).iterator();
    	while(iter.hasNext())
        {
    		String key = (String)iter.next();
            if (key.equals(getIssue().getString("key")))
                addError("Self linkage", getText("linkissue.error.selflink"));
            else if(isRecursive(key))
                addError("Recurtion detected", getText("linkissue.error.recursive"));
        }
        
        return hasAnyErrors();
    }

	private boolean hasBreakErrors()
    {
    	if(hasGeneralErrors()) return true;
    	Issue issue = JiraUtil.getIssue(issueKey);
        Iterator iter = getKeys(linkKey).iterator();
    	while(iter.hasNext())
        {
    		String key = (String)iter.next();
    		Issue linkedIssue = JiraUtil.getIssue(key);
        	if((linkDesc.equals(getIssueLinkType().getInward()) && JiraUtil.isASubTask(issue, linkedIssue)) ||
        	   (linkDesc.equals(getIssueLinkType().getOutward()) && JiraUtil.isASubTask(linkedIssue, issue)))
        	{
				addError("Cant break subtasks", getText("linkissue.error.subtask"));
				break;
			}
        }
        
        return hasAnyErrors();
    }

    private boolean hasGeneralErrors()
    {
        if (!isHasIssuePermission(Permissions.LINK_ISSUE, getIssue()))
            addErrorMessage(getText("linkissue.error.nopermission"));

        super.doValidation();
        if (!StringUtil.isBlank(linkKey))
        {
	        Iterator iter = getKeys(linkKey).iterator();
	    	while(iter.hasNext())
            {
	    		String key = (String)iter.next();
                if (ManagerFactory.getIssueManager().getIssueObject(key) == null)
                    addError("Issue not found", getText("linkissue.error.notexist", key));
            }
        }
        else
        {
            addError("Key required", getText("linkissue.error.keyrequired"));
        }

        if (!getLinkDescs().contains(linkDesc))
        {
            addError("Link description invalid", getText("linkissue.error.invalidlinkdesc"));
        }
        else if (getIssueLinkType().isSystemLinkType())
        {
            addError("System link", getText("linkissue.error.systemlink"));
        }
        
        return hasAnyErrors();
    }
}
