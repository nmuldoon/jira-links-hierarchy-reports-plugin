package com.pyxis.jira.actions;

/*
 * Copyright (c) 2007, Pyxis-Technologies inc.
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.util.Locale;
import java.util.ResourceBundle;

import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.exception.IssueNotFoundException;
import com.atlassian.jira.exception.IssuePermissionException;
import com.atlassian.jira.issue.IssueUtilsBean;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.web.action.issue.AbstractViewIssue;
import com.pyxis.jira.lhr.util.I18nUtil;
import com.pyxis.jira.lhr.util.JiraUtil;
import com.pyxis.jira.report.links.HierarchyReport;

/**
 * Abstract Action class
 * @author jchuet
 */
public abstract class AbstractPyxisAction  extends AbstractViewIssue
{    
    private static final String RESOURCE_BUNDLE = AbstractPyxisAction.class.getName();
    private ResourceBundle resourceBundle;
	protected IssueUtilsBean issueUtilsBean;

    private GenericValue issue;
    private String issueKey;
    private String bulkUID;
    private int referenceFieldId;
    
    private String decorator;
    private Boolean printView;
    private String view = HierarchyReport.ISSUE_VIEW;  

    public AbstractPyxisAction(IssueLinkManager issueLinkManager, SubTaskManager subTaskManager)
    {
    	super(issueLinkManager, subTaskManager);
    	this.issueUtilsBean = (IssueUtilsBean)ComponentManager.getComponentInstanceOfType(IssueUtilsBean.class);
    }
    
    public String doSuccess()
    {
        return SUCCESS;
    }

    public String getIssueKey()
    {
        if(issueKey != null) return issueKey;
        if(getIssueObject() != null) issueKey = getIssueObject().getKey();
        return issueKey;
    }

    public void setIssueKey(String issueKey) 
    {
        this.issueKey = issueKey != null ? issueKey.trim() : null;
        super.setKey(this.issueKey);
    }
    
    public Long getProjectId()
    {
        return getIssueObject().getProject().getLong("id");
    }

    public int getReferenceFieldId()
    {
        return referenceFieldId;
    }
    
    public void setDecorator(String decorator)
    {
        this.decorator = decorator;
    }
    
    public Boolean getPrintView()
    {
    	if(printView != null) return printView;
    	printView = decorator != null ? new Boolean(decorator.equals("printable")) : Boolean.FALSE;
    	return printView;
    }
    
    public void setPrintView(Boolean printView)
    {
    	this.printView = printView;
    }
    
    public String getBulkUID()
    {
        if(bulkUID == null) return "";
        return bulkUID;
    }

    public void setBulkUID(String bulkUID)
    {
        this.bulkUID = bulkUID;
    }

    public String getView()
    {
        return view;
    }

    public void setView(String view)
    {
        this.view = view;
    }
    
    public boolean isIssueView()
    {
        return view.equals(HierarchyReport.ISSUE_VIEW);
    }

    public boolean isHierarchyView()
    {
        return view.equals(HierarchyReport.HIERARCHY_VIEW);
    }
    
    public boolean hasErrors()
    {
        try
        {
            getIssue();
        }
        catch (IssuePermissionException e)
        {
            addError("Permission denied", getText("linkissue.error.nopermission"));
        }
        
        return hasAnyErrors();
    }
    
    @Override
    public GenericValue getIssue()
    {
        if (issue == null)
        {
            try
            {
                // todo - when we rewrite getIssue() to throw the PermissionException properly also fix getSummaryIssue()
                setIssue(getIssueManager().getIssue(getId()));

                //set the current issue (for user history)
                if (issue != null)
                {
                    //if the user doesnt have access to this issue then dont return it
                    if (!ManagerFactory.getPermissionManager().hasPermission(Permissions.BROWSE, issue, getRemoteUser()))
                    {
                        if (errorMessages == null || !errorMessages.contains(getText("admin.errors.issues.no.permission.to.see")))
                            addErrorMessage(getText("admin.errors.issues.no.permission.to.see"));
                        String issueStr = issue.getString("key") != null ? issue.getString("key") : issue.toString();
                        throw new IssuePermissionException("User '" + getRemoteUser() + "' does not have permission to see issue '" + issueStr + "'");
                    }
                }
                else
                {
                    if (errorMessages == null || !errorMessages.contains(getText("issue.wasdeleted")))
                    {
                        addErrorMessage(getText("issue.wasdeleted"));
                    }
                    throw new IssueNotFoundException("Issue with id '" + getId() + "' or key '" + getKey() + "' could not be found in the system");
                }
            }
            catch (GenericEntityException e)
            {
                log.error(e, e);
            }
        }
        return issue;
    }
    
    @Override
    public void setIssue(GenericValue issue)
    {
        this.issue = issue;
    }
    
    /**
     * Custom I18n. Based on WebWork i18n.
     * @param key
     * @return the i18nzed message. If none found key is returned.
     */
    public String getText(String key)
    {
        if (resourceBundle == null)
        {
            Locale locale = JiraUtil.getUserLocale();
            resourceBundle = ResourceBundle.getBundle(RESOURCE_BUNDLE, locale);
        }
        
        return I18nUtil.getText(key, resourceBundle);
    }
    
    public void addError(String key, String msg)
    {
        if(!getHasErrors())
            super.addError(key, msg);
    }
    
    public String getError()
    {
    	return (String)super.getErrors().values().iterator().next();
    }
}
